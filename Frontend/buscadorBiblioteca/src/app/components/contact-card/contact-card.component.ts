import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-card',
  templateUrl: './contact-card.component.html',
  styleUrls: ['./contact-card.component.scss'],
})
export class ContactCardComponent implements OnInit {
  @Input() label: string;
  @Input() icon: string;
  @Input() context: string;
  constructor() {
    this.label = '';
    this.context = '';
    this.icon = '';
  }

  ngOnInit(): void {}
}
