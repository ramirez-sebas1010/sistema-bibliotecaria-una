# Generated by Django 3.0.6 on 2020-05-25 20:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0010_auto_20200525_1551'),
    ]

    operations = [
        migrations.AlterField(
            model_name='prestamo',
            name='fecha_entrega',
            field=models.DateField(default=None, null=True),
        ),
        migrations.CreateModel(
            name='Suspension',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_inicio', models.DateField()),
                ('fecha_fin', models.DateField()),
                ('descripcion', models.CharField(max_length=140)),
                ('alumno', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.Alumno')),
            ],
        ),
        migrations.CreateModel(
            name='Amonestacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=140)),
                ('alumno', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.Alumno')),
            ],
        ),
    ]
